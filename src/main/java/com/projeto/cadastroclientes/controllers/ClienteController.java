package com.projeto.cadastroclientes.controllers;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;


import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.projeto.cadastroclientes.dtos.ClienteRecordDto;
import com.projeto.cadastroclientes.models.Cliente;
import com.projeto.cadastroclientes.repositories.ClienteRepository;

import jakarta.validation.Valid;


@RestController
public class ClienteController {

    @Autowired
    private ClienteRepository clienteRepository;

    @PostMapping("/cliente")
    public ResponseEntity<Cliente> saveCliente(@Valid @RequestBody ClienteRecordDto clienteRecordDTO) {
        Cliente cliente = new Cliente();
        BeanUtils.copyProperties(clienteRecordDTO, cliente);
        Cliente savedCliente = clienteRepository.save(cliente);
        return ResponseEntity.status(HttpStatus.CREATED).body(savedCliente);
    }
    @GetMapping("/clientes")
    public ResponseEntity<List<Cliente>> getAllClientes() {
        List<Cliente> clientes = clienteRepository.findAll();
        return ResponseEntity.status(HttpStatus.OK).body(clientes);    
    }
  
     
}
