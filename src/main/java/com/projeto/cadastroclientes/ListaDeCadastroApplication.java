package com.projeto.cadastroclientes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ListaDeCadastroApplication {

	public static void main(String[] args) {
		SpringApplication.run(ListaDeCadastroApplication.class, args);
	}

}
