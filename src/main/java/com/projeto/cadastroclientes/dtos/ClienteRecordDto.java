package com.projeto.cadastroclientes.dtos;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public record ClienteRecordDto(@NotBlank String nome, @NotNull String cpf, @NotNull String telefone) {

}
	